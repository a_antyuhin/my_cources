#include "ros/ros.h"
#include "geometry_msgs/Point.h"
#include "visualization_msgs/Marker.h"

class SubscribeAndPublish
{
public:
  SubscribeAndPublish()
  {
    //Topic you want to publish
    pub_ = n_.advertise<visualization_msgs::Marker>("output", 1);

    //Topic you want to subscribe
    sub_ = n_.subscribe("input", 1, &SubscribeAndPublish::callback, this);
  }
  //static int id_marker = 0;
  //static int count_msg = 0;
  visualization_msgs::Marker mrk;

  void callback(const geometry_msgs::Point& geo)
  {
    //visualization_msgs::Marker mrk;
    mrk.header.frame_id = "/point";
    mrk.header.stamp = ros::Time::now();
    mrk.ns = "marker_point";
    mrk.id = id_marker;
    mrk.action = visualization_msgs::Marker::ADD;
    
    mrk.type = visualization_msgs::Marker::POINTS;
    mrk.scale.x = 0.5;
    mrk.scale.y = 0.5;
    mrk.color.r = 1.0;
    mrk.color.g = 0.0;
    mrk.color.b = 0.0;
    mrk.color.a = 1.0;
    mrk.points.push_back(geo);
    //id_marker++;
    //count_msg++;
    if(mrk.points.size() == 4)
    {
        pub_.publish(mrk);
        mrk.points.clear();
        count_msg = 0;
    }
  }

private:
  ros::NodeHandle n_; 
  ros::Publisher pub_;
  ros::Subscriber sub_;

};//End of class SubscribeAndPublish

int main(int argc, char **argv)
{
  //Initiate ROS
  ros::init(argc, argv, "subscribe_and_publish");

  //Create an object of class SubscribeAndPublish that will take care of everything
  SubscribeAndPublish SAPObject;

  ros::spin();

  return 0;
}