#include <ros/ros.h>
#include <tf/transform_broadcaster.h>
#include <tf/transform_listener.h>
#include <geometry_msgs/PointStamped.h>
#include <geometry_msgs/Point.h>

class SubscribeAndPublish
{
public:
  SubscribeAndPublish()
  {
    //Topic you want to publish
    pub_ = n_.advertise<geometry_msgs::Point>("output", 100);

    //Topic you want to subscribe
    sub_ = n_.subscribe("input", 100, &SubscribeAndPublish::tfPoint, this);
  }

  void tfPoint(const geometry_msgs::PointStamped& msg)
  {
    broadcaster.sendTransform(
        tf::StampedTransform(tf::Transform(tf::Quaternion(0, 0, 0, 1), tf::Vector3(0.0, 0.0, 0.0)),
        ros::Time::now(), "core_frame", msg.header.frame_id));
    try
    {
        geometry_msgs::Point simple_point;
        listener.transformPoint("core_frame", msg, simple_point);
        pub_.publish(simple_point);
    }
    catch(tf::TransformException& ex)
    {
        ROS_ERROR("Received an exception trying to transform a point", ex.what());
    }
  }

private:
  ros::NodeHandle n_; 
  ros::Publisher pub_;
  ros::Subscriber sub_;

};

int main(int argc, char **argv)
{
  //Initiate ROS
  ros::init(argc, argv, "tfconverter_node");

  //Create an object of class SubscribeAndPublish that will take care of everything
  SubscribeAndPublish SAPObject;

  ros::spin();

  return 0;
}